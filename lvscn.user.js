// ==UserScript==
// @name        livescore navigator
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-livescore-navigator
// @downloadURL https://bitbucket.org/dsjkvf/userscript-livescore-navigator/raw/master/lvscn.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-livescore-navigator/raw/master/lvscn.user.js
// @match       *://*.livescore.com/*
// @run-at      document-start
// @version     1.0.1
// @grant       none
// ==/UserScript==


// INIT

var url = window.location.href;
var urlLastPart = url.split('/')[url.split('/').length - 2]


// HELPERS

function redirectToDate(url, delta = 0) {
    var parts = [];
    if (/^[0-9-]+$/.test(urlLastPart)) {
        parts = urlLastPart.split('-');
        parts[2] = Number(parts[2]) + delta;
        if (parts[2] < 10) {parts[2] = '0' + parts[2]};
    } else {
        var today = new Date();
        parts[2] = today.getDate();
        parts[2] = Number(parts[2]) + delta;
        if (parts[2] < 10) {parts[2] = '0' + parts[2]};
        parts[1] = today.getMonth() + 1; //January is 0!
        if (parts[1] < 10) {parts[1] = '0' + parts[1]};
        parts[0] = today.getFullYear();
    }

    var datePage = parts[0] + '-' + parts[1] + '-' + parts[2];
    return window.location.origin + 'soccer' + datePage
}

// MAIN

// Add hotkeys to navigate tips pages back an forth
addEventListener('keyup', function(e) {
    if (e.ctrlKey ||
        e.altKey ||
        e.metaKey ||
        e.shiftKey ||
        (e.which !== 74 /*j*/ && e.which !== 75 /*k*/)) {
        return;
    }
    e.preventDefault();

    if (e.which == 74) {
        window.location.replace(redirectToDate(url, -1));
    } else if (e.which == 75) {
        window.location.replace(redirectToDate(url, +1));
    }
})
