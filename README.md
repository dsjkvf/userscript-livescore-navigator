Livescore Date Navigator
========================

## About

This is a simple [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or similar extension, which -- upon visiting a front page on [Livescore](https://livescore.com/) -- will add `j/k` hotkeys to navigate the scores' pages back and forth.
